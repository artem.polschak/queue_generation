package com;

import com.csv.CsvWriter;
import com.logging.LoggerManager;
import com.model.PojoMessage;
import com.service.consumer.Consumer;
import com.service.producer.Producer;
import com.utils.LoadProperties;
import com.validate.ValidateMessage;
import org.slf4j.Logger;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.IntStream;

public class Main {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String BROKER_URL = "connection";
    private static final String QUEUE_NAME = "queueName";
    private static final String VALID_CSV_FILE_NAME = "validCSVPath";
    private static final String INVALID_CSV_FILE_NAME = "invalidCSVPath";
    private static final String TIME_AMOUNT_POISON_PILL = "timeAmount";
    private static final String MESSAGE_COUNT = "messageCount";
    private static final String TOPIC_NAME = "topicName";
    private static final String COUNT_CONSUMERS = "countConsumers";
    private static final String COUNT_VALIDATORS = "countValidators";
    private static final String POISON_PILL_MESSAGE = "poisonPillMessage";

    public static void main(String[] args) throws Exception {
        BlockingQueue<PojoMessage> pojoMessageBlockingQueue = new LinkedBlockingQueue<>(Integer.MAX_VALUE);


        LoadProperties properties = new LoadProperties();
        properties.loadProperties();


        CsvWriter csvWriter = new CsvWriter(properties.getProperty(VALID_CSV_FILE_NAME),
                properties.getProperty(INVALID_CSV_FILE_NAME));



        Thread threadProducer = new Thread(new Producer(properties.getProperty(BROKER_URL),
                properties.getProperty(QUEUE_NAME), properties.getProperty(TOPIC_NAME), getMessageCount(args, properties),
                Long.parseLong(properties.getProperty(TIME_AMOUNT_POISON_PILL)), properties.getProperty(POISON_PILL_MESSAGE),
                properties.getProperty(LOGIN), properties.getProperty(PASSWORD)));
        threadProducer.start();



        int countConsumers = Integer.parseInt(properties.getProperty(COUNT_CONSUMERS));
        IntStream.rangeClosed(1, countConsumers).forEach(i -> createThread(new Consumer(properties.getProperty(BROKER_URL)
                , properties.getProperty(QUEUE_NAME), properties.getProperty(TOPIC_NAME),
                properties.getProperty(LOGIN), properties.getProperty(PASSWORD),
                pojoMessageBlockingQueue, properties.getProperty(POISON_PILL_MESSAGE)), "ConsumerThread" + i));



        threadProducer.join();


        int countValidators = Integer.parseInt(properties.getProperty(COUNT_VALIDATORS));
        IntStream.rangeClosed(1, countValidators).forEach(i -> createThread(new ValidateMessage(pojoMessageBlockingQueue,
                csvWriter), "ValidatorThread" + i));

    }

    private static <T extends Runnable> void createThread(T runnable, String name) {
        Thread consumerThread = new Thread(runnable, name);
        consumerThread.start();
        if (runnable instanceof Consumer) {
            try {
                consumerThread.join();
            } catch (InterruptedException e) {
                LOGGER.error("Total time of program working={} {}", e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    private static int getMessageCount(String[] args, LoadProperties properties) {
        if (args.length > 0) {
            return Integer.parseInt(args[0]);
        } else {
            return Integer.parseInt(properties.getProperty(MESSAGE_COUNT));
        }
    }
}

