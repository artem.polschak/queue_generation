package com.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerManager.class);
    private LoggerManager() {}
    public static Logger getLogger() {
        return LOGGER;
    }
}
