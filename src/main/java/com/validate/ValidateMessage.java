package com.validate;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ValidatorFactory;
import java.util.concurrent.BlockingQueue;
import org.apache.activemq.util.StopWatch;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import java.util.stream.Collectors;
import com.logging.LoggerManager;
import org.slf4j.Logger;
import com.model.PojoMessage;
import java.io.IOException;
import com.csv.CsvWriter;
import java.util.Locale;
import java.util.Set;

public class ValidateMessage implements Runnable {
    private static final Logger LOGGER = LoggerManager.getLogger();
    BlockingQueue<PojoMessage> blockingQueue;
    CsvWriter csvWriter;
    int countSendMessages;
    int countValidMessages;
    int countInvalidMessages;
    Validator validator;

    public int getCountValidMessages() {
        return countValidMessages;
    }

    public int getCountInvalidMessages() {
        return countInvalidMessages;
    }
    StopWatch stopWatch;
    String threadName;
    ValidatorFactory validatorFactory;

    public ValidateMessage(BlockingQueue<PojoMessage> blockingQueue, CsvWriter csvWriter) {
        this.blockingQueue = blockingQueue;
        this.csvWriter = csvWriter;

        Locale.setDefault(Locale.ENGLISH);
    }

    void createValidatorFactory() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    public int getCountSendMessages() {
        return countSendMessages;
    }
    @Override
    public void run() {
        createValidatorFactory();
        threadName = Thread.currentThread().getName();
        stopWatch = new StopWatch(true);
        while (!blockingQueue.isEmpty()) {
            PojoMessage pojoMessage = blockingQueue.poll();
            Set<ConstraintViolation<PojoMessage>> validate = validator.validate(pojoMessage);
            if (validate.isEmpty()) {
                countValidMessages++;
                writerValidMessages(pojoMessage);
            } else {
                countInvalidMessages++;
                writeInvalidMessages(pojoMessage, validate);
            }
        }
        LOGGER.info("{} count={}", threadName, countSendMessages);
        LOGGER.info("{} RPS={}", threadName, countSendMessages / (stopWatch.taken() / 1000D));
        LOGGER.info("{} count Valid messages={}", threadName, countValidMessages);
        LOGGER.info("{} count Invalid messages={}", threadName, countInvalidMessages);
    }

    void writeInvalidMessages(PojoMessage pojoMessage, Set<ConstraintViolation<PojoMessage>> validate) {
        ObjectNode node = getJsonNodes(validate);
        pojoMessage.setErrors(node.toString());
        try {
            csvWriter.writeInvalidMessage(pojoMessage);
            countSendMessages++;

        } catch (IOException e) {
            LOGGER.error("{} Error writing invalid message: {}", threadName, e.getMessage(), e);
        }
    }

    void writerValidMessages(PojoMessage pojoMessage) {
        try {
            csvWriter.writeValidMessage(pojoMessage);
            countSendMessages++;

        } catch (IOException e) {
            LOGGER.error("{} Error writing valid message: {}", threadName, e.getMessage(), e);
        }
    }

    ObjectNode getJsonNodes(Set<ConstraintViolation<PojoMessage>> validate) {
        String error = validate.stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.joining(", "));
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();
        node.put("error", error);
        return node;
    }
}


