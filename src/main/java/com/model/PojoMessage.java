package com.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDateTime;

public class PojoMessage implements Serializable {
     String name;
     int count;
     LocalDateTime createdAt;
     String errors;

    public PojoMessage(String name, int count, LocalDateTime timestamp) {
        this.name = name;
        this.count = count;
        this.createdAt = timestamp;
        this.errors = "";
    }

    @NotNull(message = "Name is compulsory")
    @NotBlank(message = "Name is compulsory")
    @Size(min = 7, message = "Size must have minimum seven letters")
    @Pattern(regexp = "[a-zA-Z]*a[a-zA-Z]*", message = "Name must contain at least one letter 'a'")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Min(value = 10, message = "Count must be greater than or equal to 10")
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @NotNull
    @JsonDeserialize
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "PojoMessage{" +
                "name='" + name + '\'' +
                ", count=" + count +
                ", createdAt=" + createdAt +
                ", errors='" + errors + '\'' +
                '}';
    }
}
