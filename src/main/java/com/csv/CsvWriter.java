package com.csv;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.logging.LoggerManager;
import org.slf4j.Logger;

import com.model.PojoMessage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CsvWriter {
    private static final Logger LOGGER = LoggerManager.getLogger();
    File csvOutputFileValid;
    File csvOutputFileInvalid;
    CsvMapper mapper;
    CsvSchema schemaValid;
    CsvSchema schemaInvalid;

    public CsvWriter(String validCSVFile, String invalidCSVFile) throws IOException {
        this.csvOutputFileValid = new File(validCSVFile);
        this.csvOutputFileInvalid = new File(invalidCSVFile);

        this.mapper = new CsvMapper();
        this.mapper.findAndRegisterModules();
        this.mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        this.mapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);

        this.schemaValid = CsvSchema.builder().addColumns(List.of("name", "count"), CsvSchema.ColumnType.STRING).build();
        this.schemaInvalid = CsvSchema.builder().addColumns(List.of("name", "count", "errors"), CsvSchema.ColumnType.STRING).build();

        validationIfExistClean(validCSVFile, invalidCSVFile);

        writeTitle(schemaValid, csvOutputFileValid);
        writeTitle(schemaInvalid, csvOutputFileInvalid);

    }

    private void validationIfExistClean(String validCSVFile, String invalidCSVFile) throws IOException {
        if (csvOutputFileValid.exists() && csvOutputFileValid.length() > 0) {
            LOGGER.info("CSV file {} is not empty. Cleaning file...", validCSVFile);
            cleanCsvOutputFile(csvOutputFileValid);
        } else {
            LOGGER.info("CSV file {} is not exist. Creating new file...", validCSVFile);
        }
        if (csvOutputFileInvalid.exists() && csvOutputFileInvalid.length() > 0) {
            LOGGER.info("CSV file {} is not empty. Cleaning file...", invalidCSVFile);
            cleanCsvOutputFile(csvOutputFileInvalid);
        } else {
            LOGGER.info("CSV file {} is not exist. Creating new file...", invalidCSVFile);
        }
    }

    void cleanCsvOutputFile(File file) throws IOException {
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write("");
        }
    }

    void writeTitle(CsvSchema schema, File file) throws IOException {
        ObjectWriter writer = mapper.writerFor(String[].class).with(schema);
        FileWriter fileWriter = new FileWriter(file, true);
        writer.writeValue(fileWriter, schema.getColumnNames().toArray(new String[0]));
        fileWriter.close();
    }

    public void writer(PojoMessage message, CsvSchema schema, File file) throws IOException {
        ObjectWriter writer = mapper.writerFor(PojoMessage.class).with(schema);
        FileWriter fileWriter = new FileWriter(file, true);
        writer.writeValue(fileWriter, message);
        fileWriter.close();
    }

    public void writeValidMessage(PojoMessage message) throws IOException {
        writer(message, schemaValid, csvOutputFileValid);
    }

    public void writeInvalidMessage(PojoMessage message) throws IOException {
        writer(message, schemaInvalid, csvOutputFileInvalid);
    }
}




