package com.service.consumer;

import org.apache.activemq.ActiveMQConnectionFactory;
import com.logging.LoggerManager;
import org.slf4j.Logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.activemq.util.StopWatch;
import com.model.PojoMessage;

import javax.jms.*;

public class Consumer implements Runnable {
    private static final Logger LOGGER = LoggerManager.getLogger();
    BlockingQueue<PojoMessage> blockingQueue;
    ConnectionFactory connectionFactory;
    Destination destinationQueue;
    Destination destinationTopic;
    MessageConsumer consumerQueue;
    MessageConsumer consumerTopic;
    Connection connection;
    Session session;
    String baseUrl;
    AtomicInteger countReadMessages;
    String poisonPillMessage;
    String threadName;
    StopWatch stopWatch;
    String login;
    String password;
    String queueName;
    String topicName;
    AtomicBoolean topicBoolean;

    public Consumer(String brokerUrl, String queueName, String topicName,
                    String login, String password, BlockingQueue<PojoMessage> blockingQueue, String poisonPillMessage) {
        this.baseUrl = brokerUrl;
        this.blockingQueue = blockingQueue;
        this.poisonPillMessage = poisonPillMessage;
        this.login = login;
        this.password = password;
        this.queueName = queueName;
        this.topicName = topicName;
        connectionFactory = new ActiveMQConnectionFactory(baseUrl);
        /* ((ActiveMQConnectionFactory) connectionFactory).setTrustAllPackages(false);
            ((ActiveMQConnectionFactory) connectionFactory).setTrustedPackages(List.of("com.model"));*/
    }

    public AtomicBoolean getTopicBoolean() {
        return topicBoolean;
    }

    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    void createConnection() {
        try {
            connection = connectionFactory.createConnection(login, password);
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            destinationQueue = session.createQueue(queueName);
            destinationTopic = session.createTopic(topicName);
            consumerQueue = session.createConsumer(destinationQueue);
            consumerTopic = session.createConsumer(destinationTopic);
        } catch (Exception e) {
            LOGGER.error("{} error {}", threadName, e.getMessage(), e);
            close();
        }
    }

    public void run() {
        createConnection();
        threadName = Thread.currentThread().getName();
        LOGGER.info("{} Start...", threadName);
        try {
            stopWatch = new StopWatch(true);
            messageQueueListener();
            messageTopicListener();
        } catch (Exception e) {
            LOGGER.error("{} error reading messages: {}", threadName, e.getMessage(), e);
        }
    }



    int size() {
        return blockingQueue.size();
    }

    void messageQueueListener() throws JMSException {
        countReadMessages = new AtomicInteger();
        consumerQueue.setMessageListener(message -> {
            try {
                ObjectMessage msg = (ObjectMessage) message;
                PojoMessage pojoMessage = (PojoMessage) msg.getObject();
                countReadMessages.getAndIncrement();
                blockingQueue.add(pojoMessage);
            } catch (JMSException e) {
                LOGGER.error("{} method run: error {}", threadName, e.getMessage(), e);
            }
        });
    }

    void messageTopicListener() throws JMSException {
        topicBoolean = new AtomicBoolean();
        consumerTopic.setMessageListener(message -> {
            if (message instanceof TextMessage) {
                try {
                    String text = ((TextMessage) message).getText();
                    if (text.equals(poisonPillMessage)) {
                        topicBoolean.set(true);
                        LOGGER.info("{} Received POISON-PILL {}", threadName, text);
                        LOGGER.info("{} COUNT of received messages={}", threadName, countReadMessages);
                        LOGGER.info("{} RPS={}", threadName, countReadMessages.longValue() / (stopWatch.taken() / 1000D));
                        close();
                    }
                } catch (JMSException e) {
                    LOGGER.error("{} Error {}", threadName, e.getMessage(), e);
                }
            }
        });
    }

    public void close() {
        try {
            consumerQueue.close();
            consumerTopic.close();
            session.close();
            connection.close();
            LOGGER.info("{} , session, connection are closed", threadName);
        } catch (Exception e) {
            LOGGER.error("{} Error closing producer, session, or connection: {}", threadName, e.getMessage(), e);
        }
    }
}



