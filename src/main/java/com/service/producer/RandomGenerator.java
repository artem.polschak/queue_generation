package com.service.producer;

import org.apache.commons.lang3.RandomStringUtils;
import java.time.LocalDateTime;
import java.util.concurrent.ThreadLocalRandom;

public class RandomGenerator {
     static final int MIN_NAME_SIZE = 1;
     static final int MAX_NAME_SIZE = 40;
     static final int MIN_COUNT_SIZE = 1;
     static final int MAX_COUNT_SIZE = 99;

    private RandomGenerator() {}
    public static String getRandomString() {
        int length = ThreadLocalRandom.current().nextInt(MIN_NAME_SIZE, MAX_NAME_SIZE);
        return RandomStringUtils.randomAlphabetic(length);
    }
    public static int getRandomCount() {
        return ThreadLocalRandom.current().nextInt(MIN_COUNT_SIZE, MAX_COUNT_SIZE);
    }
    public static LocalDateTime getRandomDate() {
        /*long minDay = LocalDate.of(1970, 1, 1).toEpochDay();
        long maxDay = LocalDate.now().toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        LocalDateTime randomDateTime = LocalDateTime.of(LocalDate.ofEpochDay(randomDay), LocalTime.NOON);
        long randomSeconds = ThreadLocalRandom.current().nextLong(0, ChronoUnit.DAYS.getDuration().getSeconds());
        return randomDateTime.plus(randomSeconds, ChronoUnit.SECONDS);*/
       return LocalDateTime.now();
    }
}

