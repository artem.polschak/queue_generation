package com.service.producer;

import com.logging.LoggerManager;
import com.model.PojoMessage;
import org.slf4j.Logger;
import javax.jms.*;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;
import static com.service.producer.RandomGenerator.*;
public class MessageSender {
    private static final Logger LOGGER = LoggerManager.getLogger();
    Session session;
    MessageProducer producerQueue;

    public MessageSender(Session session, MessageProducer producerQueue) {
        this.session = session;
        this.producerQueue = producerQueue;
    }

    AtomicInteger sendMessages(long timeout, int amountMessagesToSend) {
        LocalDateTime startTime = LocalDateTime.now();
        LOGGER.info("ProducerThread start...");

        AtomicInteger countSentMessages = new AtomicInteger();
        Stream.generate(() -> new PojoMessage(getRandomString(), getRandomCount(), getRandomDate()))
                .takeWhile(o -> LocalDateTime.now().isBefore(startTime.plusSeconds(timeout)))
                .limit(amountMessagesToSend).forEach(pojoMessage -> {
                    try {
                        ObjectMessage message = session.createObjectMessage(pojoMessage);
                        producerQueue.send(message);
                        countSentMessages.incrementAndGet();

                    } catch (JMSException e) {
                        LOGGER.error("PRODUCER Error sending message: {}", e.getMessage(), e);
                    }
                });
        return countSentMessages;
    }
}
