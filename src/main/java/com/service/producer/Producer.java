package com.service.producer;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.util.StopWatch;
import com.logging.LoggerManager;
import org.slf4j.Logger;
import javax.jms.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Producer implements Runnable {
    private static final Logger LOGGER = LoggerManager.getLogger();
    int amountMessagesToSend;
    AtomicInteger countSentMessages;
    String baseUrl;
    long timeAmount;
    ConnectionFactory connectionFactory;
    Connection connection;
    Session session;
    Destination destinationQueue;
    Destination destinationTopic;
    MessageProducer producerQueue;
    MessageProducer producerTopic;
    String poisonPillMessage;
    Message finishMessage;
    String login;
    String password;
    String queueName;
    String topicName;

    public Producer(String brokerUrl, String queueName, String topicName, int amountMessagesToSend, long timeAmount,
                    String poisonPillMessage, String login, String password) {
        this.amountMessagesToSend = amountMessagesToSend;
        this.timeAmount = timeAmount;
        this.baseUrl = brokerUrl;
        this.poisonPillMessage = poisonPillMessage;
        this.login = login;
        this.password = password;
        this.queueName = queueName;
        this.topicName = topicName;
        this.connectionFactory = new ActiveMQConnectionFactory(baseUrl);
        /*((ActiveMQConnectionFactory) connectionFactory).setTrustAllPackages(false);
            ((ActiveMQConnectionFactory) connectionFactory).setTrustedPackages(List.of("com.model"));*/
    }

    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public void run() {
        createConnection();
        StopWatch watch = new StopWatch(true);
        MessageSender sender = new MessageSender(session, producerQueue);
        countSentMessages = sender.sendMessages(timeAmount, amountMessagesToSend);
        close();

        LOGGER.info("PRODUCER COUNT of sent messages={}", countSentMessages);
        LOGGER.info("PRODUCER RPS={}", countSentMessages.longValue() / (watch.taken() / 1000D));
    }

    public void createConnection() {
        try {
            this.connection = connectionFactory.createConnection(login, password);
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            connection.start();

            destinationQueue = session.createQueue(queueName);
            destinationTopic = session.createTopic(topicName);

            producerQueue = session.createProducer(destinationQueue);
            producerTopic = session.createProducer(destinationTopic);

            producerQueue.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        } catch (Exception e) {
            LOGGER.error("PRODUCER Error with connection: {}", e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    public void close() {
        try {
            finishMessage = session.createTextMessage(poisonPillMessage);
            producerTopic.send(finishMessage);
            producerTopic.close();
            producerQueue.close();
            session.close();
            connection.close();
            LOGGER.info("PRODUCER Producer, session, connection are closed");
        } catch (Exception e) {
            LOGGER.error("PRODUCER Error closing producer, session, or connection: {}", e.getMessage(), e);
        }
    }
}

