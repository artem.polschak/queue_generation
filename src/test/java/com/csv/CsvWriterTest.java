package com.csv;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.model.PojoMessage;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
class CsvWriterTest {
    @Mock
    private FileWriter fileWriter;

    @Mock
    private File csvOutputFileValid;
    @Mock
    private File csvOutputFileInvalid;
    @Mock
    private CsvMapper mapper;
    @Mock
    private CsvSchema schemaValid;
    @Mock
    private CsvSchema schemaInvalid;

    private CsvWriter csvWriter;

    private final String validCSVFile = "valid1.csv";
    private final String invalidCSVFile = "invalid1.csv";

    @BeforeEach
    void setup() throws IOException {
        MockitoAnnotations.openMocks(this);
        csvWriter = new CsvWriter(validCSVFile, invalidCSVFile);
    }

    @Test
    void testCleanFile() throws IOException {
        File invalidFile = new File(invalidCSVFile);
        File validFile = new File(validCSVFile);

        try (FileWriter fileWriterInvalid = new FileWriter(invalidFile);
             FileWriter fileWriterValid = new FileWriter(validFile)) {
            fileWriterInvalid.write("data");
            fileWriterValid.write("data");
        }

        assertTrue(invalidFile.exists());
        assertTrue(validFile.exists());
        assertTrue(invalidFile.length() > 0);
        assertTrue(validFile.length() > 0);

        csvWriter.cleanCsvOutputFile(invalidFile);
        csvWriter.cleanCsvOutputFile(validFile);

        assertTrue(invalidFile.exists());
        assertEquals(0, invalidFile.length());

        assertTrue(validFile.exists());
        assertEquals(0, validFile.length());

        assertTrue(invalidFile.delete());
        assertTrue(validFile.delete());
    }
}

