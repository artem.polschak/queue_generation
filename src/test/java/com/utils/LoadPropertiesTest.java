package com.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.*;

class LoadPropertiesTest {
    private LoadProperties loadProperties;

    @BeforeEach
    void setUp() {
        loadProperties = new LoadProperties();
    }

    @Test
    void getPropertyShouldReturnCorrectValue() throws IOException {
        loadProperties.loadProperties();
        String value = loadProperties.getProperty("poisonPillMessage");
        assertThat(value).isNotNull().isEqualTo("Poison-pill");
    }
    @Test
    void loadPropertiesKeyNotExist() throws IOException {
        loadProperties.loadProperties();
        String value = loadProperties.getProperty("test");
        assertThat(value).isNull();
    }
}