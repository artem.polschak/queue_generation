package com.service.consumer;

import com.model.PojoMessage;
import com.service.producer.Producer;
import com.utils.LoadProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import javax.jms.JMSException;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ConsumerTest {

    @Test
    void messageQueueListenerTest() throws JMSException, InterruptedException, IOException {

        LoadProperties properties = new LoadProperties();
        properties.loadProperties();

        BlockingQueue<PojoMessage> queue = new LinkedBlockingQueue<>();
        Producer producer = new Producer(properties.getProperty("connection"), "queueName1",
                "topicName1", 1, 1000L, "poisonPillMessage",
                "login", "password");
        producer.run();
        Consumer consumer = new Consumer(properties.getProperty("connection"), "queueName1",
                "topicName1", "login", "password", queue, "poisonPillMessage");
        consumer.createConnection();
        consumer.messageQueueListener();
        Thread.sleep(1000);
        assertThat(consumer.size()).isEqualTo(1);
    }

    @Test
    void messageTopicListenerTest() throws IOException {
        LoadProperties properties = new LoadProperties();
        properties.loadProperties();

        BlockingQueue<PojoMessage> queue = new LinkedBlockingQueue<>();
        Producer producer = new Producer(properties.getProperty("connection"), "queueName2",
                "topicName2", 10, 1000L, "Poison-pill",
                "login", "password");
        Consumer consumer =  new Consumer(properties.getProperty("connection"), "queueName2",
                "topicName2", "login", "password", queue, "Poison-pill");
        consumer.run();
        producer.run();
        assertThat(consumer.getTopicBoolean()).isTrue();
    }
}