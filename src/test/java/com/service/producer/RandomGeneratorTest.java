package com.service.producer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

class RandomGeneratorTest {

    @Test
    void getRandomString_shouldReturnRandomStringWithinSizeRange() {
        String randomString = RandomGenerator.getRandomString();
        int length = randomString.length();
        Assertions.assertTrue(length >= RandomGenerator.MIN_NAME_SIZE && length < RandomGenerator.MAX_NAME_SIZE,
                "Length of random string should be within the specified range");
    }

    @Test
    void getRandomCount_shouldReturnRandomCountWithinRange() {
        int randomCount = RandomGenerator.getRandomCount();
        Assertions.assertTrue(randomCount >= RandomGenerator.MIN_COUNT_SIZE && randomCount < RandomGenerator.MAX_COUNT_SIZE,
                "Random count should be within the specified range");
    }

    @Test
    void getRandomDate_shouldReturnCurrentDateTime() {
        LocalDateTime randomDate = RandomGenerator.getRandomDate();
        LocalDateTime currentDateTime = LocalDateTime.now();
        Assertions.assertEquals(currentDateTime, randomDate,
                "Random date should be equal to the current date and time");
    }


}
