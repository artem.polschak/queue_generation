package com.service.producer;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jms.MessageProducer;
import javax.jms.Session;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertThrows;

@ExtendWith(MockitoExtension.class)
class MessageSenderTest {
    @Mock
    MessageProducer producer;

    @Mock
    Session session;

    @Test
    void messagesSender() {
       /* MessageProducer producer = Mockito.mock(MessageProducer.class);
        Session session = Mockito.mock(Session.class);*/

        AtomicInteger result = new MessageSender(session, producer)
                .sendMessages(1, 10);
        Assertions.assertEquals(10, result.intValue());
    }
}