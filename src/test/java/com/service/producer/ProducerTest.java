package com.service.producer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import javax.jms.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ProducerTest {
    @Mock
    private ConnectionFactory connectionFactory;
    @Mock
    private Connection connection;
    @Mock
    private Session session;
    @Mock
    private MessageProducer producerQueue;
    @Mock
    private MessageProducer producerTopic;
    @Mock
    private Queue destinationQueue;
    @Mock
    private Topic destinationTopic;
    @Test
    void testProducer() throws JMSException {
        Producer producer = new Producer("baseUrl", "queueName",
                "topicName", 10, 1000L, "poisonPillMessage",
                "login", "password");
        producer.setConnectionFactory(connectionFactory);
        when(connectionFactory.createConnection("login", "password")).thenReturn(connection );
        when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenReturn(session);
        when(session.createQueue("queueName")).thenReturn(destinationQueue);
        when(session.createTopic("topicName")).thenReturn(destinationTopic);
        when(session.createProducer(destinationQueue)).thenReturn(producerQueue);
        when(session.createProducer(destinationTopic)).thenReturn(producerTopic);

        producer.run();

        assertThat(producer.amountMessagesToSend).isEqualTo(producer.countSentMessages.intValue())
                .isEven().isPositive()
                .isLessThan(11).isBetween(1, 20);
        verify(session).close();
    }

    @Test
    void testRun() throws JMSException {
        String brokerUrl = "tcp://localhost:61616";
        String queueName = "testQueue";
        String topicName = "testTopic";
        int amountMessagesToSend = 10;
        long timeAmount = 1000L;
        String poisonPillMessage = "poison-pill";
        String login = "login";
        String password = "password";

        Producer producer = new Producer(brokerUrl, queueName,
                topicName, amountMessagesToSend, timeAmount, poisonPillMessage,
                login, password);
        producer.setConnectionFactory(connectionFactory);
        when(connectionFactory.createConnection(login, password)).thenReturn(connection );
        when(connection.createSession(false, Session.AUTO_ACKNOWLEDGE)).thenReturn(session);
        when(session.createQueue(queueName)).thenReturn(destinationQueue);
        when(session.createTopic(topicName)).thenReturn(destinationTopic);
        when(session.createProducer(destinationQueue)).thenReturn(producerQueue);
        when(session.createProducer(destinationTopic)).thenReturn(producerTopic);

        producer.run();

        verify(connectionFactory).createConnection(login, password);
        verify(connection).start();
        verify(session).createQueue(queueName);
        verify(session).createTopic(topicName);
        verify(session).createProducer(destinationQueue);
        verify(session).createProducer(destinationTopic);
        verify(producerQueue).setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        verify(producerQueue).close();
        verify(producerTopic).close();
        verify(session).close();
        verify(connection).close();
    }

    @Test
    void createConnection_ThrowsRuntimeExceptionOnError() {
        String brokerUrl = "invalid-url";
        String queueName = "testQueue";
        String topicName = "testTopic";
        int amountMessagesToSend = 10;
        long timeAmount = 1000;
        String poisonPillMessage = "Poison Pill";
        String login = "username";
        String password = "password";

        Producer producer = new Producer(brokerUrl, queueName, topicName,
                amountMessagesToSend, timeAmount,
                poisonPillMessage, login, password);

        assertThrows(RuntimeException.class, producer::createConnection);
    }
}
