package com.validate;

import com.csv.CsvWriter;
import com.model.PojoMessage;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import java.time.LocalDateTime;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ValidateMessageTest {
    @Mock
    private CsvWriter csvWriter;


    @Test
    void myTest() throws InterruptedException {
        BlockingQueue<PojoMessage> blockingQueue = new LinkedBlockingQueue<>();
        PojoMessage pojoMessage = new PojoMessage("aaa", 1, null);
        blockingQueue.put(pojoMessage);

        ValidateMessage validateMessage = new ValidateMessage(blockingQueue, csvWriter);
        validateMessage.run();
        assertThat(validateMessage.getCountSendMessages()).isEqualTo(1);
        assertThat(validateMessage.getCountInvalidMessages()).isEqualTo(1);
        assertThat(validateMessage.getCountValidMessages()).isZero();
    }

    @Test
    void run_WhenBlockingQueueContainsValidMessage_ShouldCallWriteValidMessage() throws InterruptedException {
        BlockingQueue<PojoMessage> blockingQueue = new LinkedBlockingQueue<>();
        PojoMessage pojoMessage = new PojoMessage("aIqlOoDsRfhx", 56, LocalDateTime.now());
        PojoMessage pojoMessage1 = new PojoMessage("fdf", 1, null);
        blockingQueue.put(pojoMessage);
        blockingQueue.put(pojoMessage1);
        ValidateMessage validateMessage = new ValidateMessage(blockingQueue, csvWriter);
        validateMessage.run();
        assertThat(validateMessage.getCountSendMessages()).isEqualTo(2);
        assertThat(validateMessage.getCountInvalidMessages()).isEqualTo(1);
        assertThat(validateMessage.getCountValidMessages()).isEqualTo(1);
    }
}


